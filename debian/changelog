monero (0.18.3.4+~0+20200826-3) unstable; urgency=medium

  * Use fully qualified names for epee
  * Add missing B-D python3-zmq

 -- Bastian Germann <bage@debian.org>  Tue, 18 Feb 2025 18:50:20 +0100

monero (0.18.3.4+~0+20200826-2) unstable; urgency=medium

  * Replace non-forwarded patch with a PR patch
  * Add Skein public-domain notice
  * Add general copyright for upstream patches
  * Drop kfreebsd reference
  * Backport CVE-2025-26819 fix (Closes: #1098240)

 -- Bastian Germann <bage@debian.org>  Tue, 18 Feb 2025 11:51:53 +0100

monero (0.18.3.4+~0+20200826-1) unstable; urgency=medium

  * New upstream version 0.18.3.4

 -- Bastian Germann <bage@debian.org>  Wed, 13 Nov 2024 20:02:52 +0000

monero (0.18.3.1+~0+20200826-2) unstable; urgency=medium

  * Team upload
  * Patch: add support for miniupnpc API version 18 (Closes: #1076690)
  * Patch: missing header to build with GCC 14 (Closes: #1076690)

 -- Bastian Germann <bage@debian.org>  Mon, 29 Jul 2024 22:45:55 +0000

monero (0.18.3.1+~0+20200826-1) unstable; urgency=medium

  * Team upload
  * Drop upstream patch
  * New upstream version 0.18.3.1+~0+20200826

 -- Bastian Germann <bage@debian.org>  Sun, 08 Oct 2023 22:09:43 +0200

monero (0.18.2.2+~0+20200826-2) unstable; urgency=medium

  * Patch: fix missing <cstdint> includes (Closes: #1037781)

 -- Bastian Germann <bage@debian.org>  Thu, 06 Jul 2023 17:54:07 +0200

monero (0.18.2.2+~0+20200826-1) unstable; urgency=medium

  * New upstream version 0.18.2.2+~0+20200826
  * Install python also for nocheck (Closes: #1032652)

  [ Graham Inggs ]
  * Build with -O1 on riscv64 to avoid link error (LP: #1940505)

 -- Bastian Germann <bage@debian.org>  Tue, 13 Jun 2023 12:21:18 +0200

monero (0.18.0.0+~0+20200826-1) unstable; urgency=medium

  * New upstream version 0.18.0.0+~0+20200826
  * Drop readline
  * Rebase patches to new upstream version
  * Functional tests need montonic and psutil python modules
  * Replace qtchooser with qttools5-dev-tools
  * Drop BSD-3-clause~Hildenborg license (not contained anymore)

 -- Bastian Germann <bage@debian.org>  Tue, 26 Jul 2022 22:15:22 +0200

monero (0.17.3.2+~0+20200826-2) unstable; urgency=medium

  * gbp: Remove unnecessary filter
  * d/copyright: Remove unused patch reference

 -- Bastian Germann <bage@debian.org>  Thu, 14 Jul 2022 12:23:13 +0200

monero (0.17.3.2+~0+20200826-1) unstable; urgency=medium

  * Revert "orphan package: set maintainer to Debian QA Group"
  * Adopt package (Closes: #1012557)
  * Let CMake ignore git submodules
  * New upstream version 0.17.3.2+~0+20200826 (Closes: #1013532)

 -- Bastian Germann <bage@debian.org>  Tue, 28 Jun 2022 18:21:50 +0200

monero (0.17.3.0+~0+20200826-2) unstable; urgency=medium

  * declare compliance with Debian Policy 4.6.1
  * orphan package: set maintainer to Debian QA Group

 -- Jonas Smedegaard <dr@jones.dk>  Thu, 09 Jun 2022 10:41:55 +0200

monero (0.17.3.0+~0+20200826-1) unstable; urgency=medium

  [ upstream ]
  * new release

  [ Jonas Smedegaard ]
  * update git-buildpackage config:
    + use DEP-14 branch names debian/latest upstream/latest
      (not master upstream)
    + enable automatic DEP-14 branch name handling
    + add usage comment
  * update copyright info:
    + update coverage
    + use Reference field (not License-Reference);
      tighten lintian overrides
  * use -g0 on mipsel to fix FTBFS;
    closes: bug#1010097, thanks to Sebastian Ramacher and Adrian Bunk
  * update storage needs in long description,
    thanks to Arnaud Rebillout
  * drop patch 2003 adopted upstream
  * unfuzz patches

 -- Jonas Smedegaard <dr@jones.dk>  Sun, 24 Apr 2022 20:46:46 +0200

monero (0.17.2.0+~0+20200826-1) unstable; urgency=medium

  [ upstream ]
  * new release

  [ Jonas Smedegaard ]
  * copyright info: fix main source URI
  * update Homepage URI

 -- Jonas Smedegaard <dr@jones.dk>  Tue, 11 May 2021 19:27:41 +0200

monero (0.17.1.9+~0+20200826-1) unstable; urgency=medium

  [ upstream ]
  * new release(s)

  [ Jonas Smedegaard ]
  * unfuzz patches
  * modernize source script copyright-check
  * copyright:
    + update coverage
    + update source URL
  * embed supercop, and wiggle it into place during build
  * add patch 2003 to fix Boost build failure;
    closes: bug#973196,
    thanks to Lucas Nussbaum, Xiphon, ErCiccione and MJ
  * declare compliance with Debian Policy 4.5.1
  * use debhelper compatibility level 13 (not 12)
  * unfuzz patch 2001
  * update TODOs

 -- Jonas Smedegaard <dr@jones.dk>  Wed, 27 Jan 2021 14:23:06 +0100

monero (0.16.0.0-2) unstable; urgency=medium

  * skip heavy test on architectures without FPU in baseline spec;
    closes: bug#962250, thanks to Adrian Bunk
  * fix support DEB_BUILD_OPTIONS=nocheck

 -- Jonas Smedegaard <dr@jones.dk>  Sat, 06 Jun 2020 14:30:46 +0200

monero (0.16.0.0-1) unstable; urgency=medium

  [ upstream ]
  * new release

  [ Jonas Smedegaard ]
  * copyright: update coverage
  * drop patch cherry-picked from upstream merge-request since adopted
  * unfuzz and update patches

 -- Jonas Smedegaard <dr@jones.dk>  Tue, 02 Jun 2020 15:14:43 +0200

monero (0.15.0.5-2) unstable; urgency=medium

  * fix link with libatomic on armel/mipsel/powerpc;
    see bug#961100, thanks to Adrian Bunk

 -- Jonas Smedegaard <dr@jones.dk>  Wed, 20 May 2020 16:06:30 +0200

monero (0.15.0.5-1) unstable; urgency=medium

  [ upstream ]
  * new release

  [ Jonas Smedegaard ]
  * declare compliance with Debian Policy 4.5.0
  * watch: set dversionmangle=auto
  * use debhelper compatibility level 12 (not 10);
    build-depend on debhelper-compat (not debhelper)
  * add patch cherry-picked (proposed) upstream
    to fix missing test name in unit test
  * have dh_dwz skip file failing with "Unknown DWARF DW_OP_255";
    see bug#949296

 -- Jonas Smedegaard <dr@jones.dk>  Fri, 20 Mar 2020 12:06:10 +0100

monero (0.15.0.1-2) unstable; urgency=medium

  * fix pass cmake the full path to Python3 executable,
    and more reliably exclude failing tests;
    fix build-depend on python3-requests
  * add patch 2002 to avoid privacy breach in documentation
  * use variable $AUTOPKGTEST_TMP (not $ADTTMP)

 -- Jonas Smedegaard <dr@jones.dk>  Sun, 15 Dec 2019 00:26:22 +0100

monero (0.15.0.1-1) experimental; urgency=medium

  [ upstream ]
  * new release(s)

  * declare compliance with Debian Policy 4.4.1
  * include documentation as html and plaintext;
    build-depend on pandoc
  * copyright: drop file no longer shipped upstream
  * drop patch cherry-picked upstream since applied
  * unfuzz patches
  * bump debhelper compatibility level to 10;
    stop explicitly enable parallel build
  * extend patch 2001 to cover linking with system shared librandomx;
    build-depend on librandomx-dev

 -- Jonas Smedegaard <dr@jones.dk>  Sat, 14 Dec 2019 11:21:54 +0100

monero (0.14.1.2-2) unstable; urgency=medium

  * Add patch cherry-picked upstream
    to fix check for disconnecting peers when syncing.
  * Declare compliance with Debian Policy 4.4.0.

 -- Jonas Smedegaard <dr@jones.dk>  Sat, 07 Sep 2019 10:45:07 +0200

monero (0.14.1.2-1) unstable; urgency=medium

  [ upstream ]
  * New release(s).
    + Fix compiling with GCC 9.1.
      Closes: Bug#925780. Thanks to Matthias Klose.

  [ Jonas Smedegaard ]
  * Unfuzz patches.
  * Improve compiler flags for 32bit archs.
    Closes: Bug#910517. Thanks to Emilio Pozuelo Monfort and Adrian Bunk.
  * Improve testsuite:
    + Tell cmake to use python3 for tests.
      Build-depend on python3.
    + Enable test block_weight.
    + Skip test hash-slow-4 segfaulting.

 -- Jonas Smedegaard <dr@jones.dk>  Tue, 20 Aug 2019 09:25:04 +0200

monero (0.14.0.2-1) unstable; urgency=medium

  * Fix apply debhelper options also when overriding.
  * Skip tests possibly broken:
    + cnv4-jit (SEGFAULT)
    + block_weight (BAD_COMMAND)
  * Skip slow and broken tests in autopkgtests.

 -- Jonas Smedegaard <dr@jones.dk>  Thu, 23 May 2019 13:20:10 +0200

monero (0.14.0.2-1~exp1) experimental; urgency=medium

  [ upstream ]
  * New release(s).

  [ Jonas Smedegaard ]
  * Fix DEP3 patch headers.
  * Update watch file:
    + Generalize version mangling for repackaging.
    + Simplify regular expressions.
    + Rewrite usage comment.
  * Update copyright info:
    + Extend coverage for main upstream author.
    + Extend coverage of packaging.
  * Drop patches cherry-picked upstream now applied.
  * Refresh patch 2001.
  * Ignore testsuite failures when targeted experimental.

 -- Jonas Smedegaard <dr@jones.dk>  Fri, 22 Mar 2019 17:58:51 +0100

monero (0.13.0.4-2) unstable; urgency=medium

  * Add patches cherry-picked upstream to fix build on big-endian archs.

 -- Jonas Smedegaard <dr@jones.dk>  Tue, 20 Nov 2018 17:11:58 +0100

monero (0.13.0.4-1) unstable; urgency=medium

  [ upstream ]
  * New release.

  [ Jonas Smedegaard ]
  * Update package relations:
    + Build-depend on libhidapi-dev only on non-kfreebsd and non-hurd.

 -- Jonas Smedegaard <dr@jones.dk>  Thu, 25 Oct 2018 01:40:16 +0200

monero (0.13.0.3-1) unstable; urgency=high

    + Supports Monero network hardfork.
      Closes: Bug#911463, #911466. Thanks to Hypertropho.

  [ Jonas Smedegaard ]
  * Update copyright info:
    + Stop repackage source: Upstream no longer ships non-free files.
    + Track autotools files.
    + Track cmake script licensed BSL-1.0.
  * Drop patch cherry-picked upstream and now applied.
  * Unfuzz and update patch 2001.
  * Update package reations:
    + Build-depend on libhidapi-dev (not libpcsclite-dev).
    + Build-depend on libgtest-dev libsodium-dev.
  * Set urgency=high due to Monero network hardfork.
  * Add patch 1001 to avoid non-UTF8 chars
    maybe causing out of memory in gcc.
  * Have autopkgtest skip two more DNSSEC tests.

 -- Jonas Smedegaard <dr@jones.dk>  Sat, 20 Oct 2018 18:27:13 +0200

monero (0.12.3.0~dfsg-2) unstable; urgency=medium

  * Fix autopkgtest.
  * Add patch cherry-picked upstream to add new DNSSEC trust anchor for
    rollover.
    Closes: Bug#909021. Thanks to Santiago R.R.
  * Declare compliance with Debian Policy 4.2.1.

 -- Jonas Smedegaard <dr@jones.dk>  Tue, 02 Oct 2018 15:33:55 +0200

monero (0.12.3.0~dfsg-1) unstable; urgency=medium

  [ upstream ]
  * New release.

  [ Jonas Smedegaard ]
  * Improve long description, to talk about project first and specific
    package parts last.
  * Declare source package as not requiring root to build.
  * Declare compliance with Debian Policy 4.1.5.
  * Improve avoiding tests requiring network:
    + Set GTEST_FILTER.
    + Drop patch 2002.
    Thanks to upstream Monero developers.
  * Improve testing:
    + Add binary package monero-tests.
    + Add autopkgtest, using newly added package monero-tests.
    + Emit testsuite details only on error.
    + Avoid core_tests during build (only in autopkgtest).
  * Build-depend on libdb-dev.

 -- Jonas Smedegaard <dr@jones.dk>  Sat, 07 Jul 2018 14:31:55 +0200

monero (0.12.2.0~dfsg-3) unstable; urgency=medium

  * Avoid building with -maes: Horribly handled for non-x86 archs, and
    not generally supported even on amd64.

 -- Jonas Smedegaard <dr@jones.dk>  Thu, 21 Jun 2018 09:54:34 +0200

monero (0.12.2.0~dfsg-2) unstable; urgency=medium

  * Update copyright info: Fix list files copyright Google.
    Closes: Bug#901915. Thanks to Chris Lamb.
  * Update package relations:
    + Build-depend on qtchooser, needed to generate translation files.
    + Stop build-depend on libunwind-dev: Unneeded (internal fork of
      easylogging++ used instead).

 -- Jonas Smedegaard <dr@jones.dk>  Wed, 20 Jun 2018 21:51:20 +0200

monero (0.12.2.0~dfsg-1) unstable; urgency=low

    * Initial Release.
      Closes: bug#890974. Thanks to Guido Günther and Subgraph.

 -- Jonas Smedegaard <dr@jones.dk>  Tue, 19 Jun 2018 11:26:34 +0200
